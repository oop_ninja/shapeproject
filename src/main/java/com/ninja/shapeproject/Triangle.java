/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ninja.shapeproject;

/**
 *
 * @author USER
 */
public class Triangle {
    private double b;
    private double h;
    public static final double half=1.0/2;
    public Triangle (double b,double h){
        this.b=b;
        this.h=h;
    } 
    public double calArea(){
        return 1.0/2*b*h;
    } 
    public double getB(){
        return b;
    }
    public double getH(){
        return h;
    }
    public void setBH(double b,double h){
        if(b<=0){
            System.out.println("Error: base must more than zero!!!");
            return;
        }
        if(h<=0){
            System.out.println("Error: height must more than zero!!!");
            return;
        }
        this.b=b;
        this.h=h;
    }
}

